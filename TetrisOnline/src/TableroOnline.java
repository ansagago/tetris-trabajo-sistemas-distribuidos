import java.awt.event.KeyEvent;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Tablero que espera a conectarse a un cliente de la aplicaci�n e imita su funcionamiento
 */

public class TableroOnline extends Tablero{
	private Socket s=null;
	private ServerSocket ss=null;
	private DataInputStream dis = null;
	
	public TableroOnline() {
		super();
		siguientePieza();
		
		ExecutorService pool = Executors.newFixedThreadPool(2);
		Runnable r = new Runnable() {
			@Override
			public void run() {
				InputListen();
			}
		};
		pool.execute(r);
	}
	
	/*
	 * Recibe la tecla pulsada desde un TableroJugador y act�a de manera similar a como lo hace este
	 */
	public void InputListen() {
		ExecutorService pool = null;
		ServerSocket ss = null;
		
		try {
			pool = Executors.newCachedThreadPool();
			ss = new ServerSocket(8888);
			
			while(true) {
				final Socket s = ss.accept();
				Runnable r = new Runnable() {
					
					@Override
					public void run() {
						DataInputStream dis = null;
						
						try {
							dis = new DataInputStream(s.getInputStream());
							int key = dis.readInt();
							int keyEventType = dis.readInt();
							
							if(keyEventType == KeyEvent.KEY_PRESSED) {
								if(key == KeyEvent.VK_LEFT) {
									getActual().setMovX(-1);
								}
								else if(key == KeyEvent.VK_RIGHT) {
									getActual().setMovX(1);
								}
								else if(key == KeyEvent.VK_DOWN) {
									getActual().bajarPieza();
								}
							}
							if(keyEventType == KeyEvent.KEY_RELEASED) {
								if(key == KeyEvent.VK_DOWN) {
									getActual().pararBajarPieza();
								}
								else if(key == KeyEvent.VK_UP) {
									getActual().rotar();
								}
							}
							
							
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				};
				
				pool.execute(r);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(ss != null) ss.close();
				if(pool != null) pool.shutdown();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Recibe siguiente pieza que generar� el TableroJugador y genera la misma
	 * Actualiza el valor de la puntuaci�n del jugador remoto
	 * Marca fin si no hay m�s espacio para crear una nueva pieza
	 */
	@Override
	public void siguientePieza() {
		

		try {
			if(ss==null) {
				ss = new ServerSocket(7777);
			}
			if(s==null) {
				s = ss.accept();
			}
			if(dis==null) {
				dis = new DataInputStream(s.getInputStream());
			}
			
			int numero=dis.readInt();
			this.puntuacion=dis.readInt();
			
		
		Tetrimino nuevaPieza = getNuevaPieza(numero);
		
		for(int i = 0; i<nuevaPieza.getCoords().length;i++) {
			for(int j=0; j<nuevaPieza.getCoords()[i].length;j++) {
				if(nuevaPieza.getCoords()[i][j] != 0) {
					if(getTablero()[i][j+4] != 0) { //Si no hay hueco para una nueva pieza
						setFin(true);
						return; //Para no crear una nueva pieza
					}
				}
			}
		}
		setActual(nuevaPieza);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Recibe y copia el estado del TableroJugador remoto para evitar desincronizaciones entre 
	 * 		las piezas que hayan podido ser causadas por problemas de conexi�n
	 */
	public void envioColisionPieza() {
		try {
			if(ss==null) {
				ss = new ServerSocket(7777);
			}
			if(s==null) {
				s = ss.accept();
			}
			
			ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
			setTablero((int[][])ois.readObject());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		
	}

	/*
	 * Crea un nuevo Tetrimino
	 */
	@Override
	public Tetrimino getNuevaPieza(int numero) {
		Tetrimino t = null;
		switch(numero) {
			case 0: //I
				t = new Tetrimino(bloques.getSubimage(0, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1,1}},this); 
				break;
			case 1: //Z
				t = new Tetrimino(bloques.getSubimage(tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,0},{0,1,1}},this); 
				break;
			case 2: //S
				t = new Tetrimino(bloques.getSubimage(2*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{0,1,1},{1,1,0}},this);
				break;
			case 3: //J
				t = new Tetrimino(bloques.getSubimage(3*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{0,0,1}},this); 
				break;
			case 4: //L
				t = new Tetrimino(bloques.getSubimage(4*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{1,0,0}},this); 
				break;
			case 5: //T
				t = new Tetrimino(bloques.getSubimage(5*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{0,1,0}},this); 
				break;
			case 6: //O
				t = new Tetrimino(bloques.getSubimage(6*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1},{1,1}},this); 
				break;
		}
		return t;
	}

}
