import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

/*
 * Ventana que crea y muestra un tablero tableroJugador utilizable por el jugador
 */

public class Ventana {
	
	private JFrame ventana;
	public static final int ANCHO = 340;
	public static final int ALTO = 700;
	private TableroJugador tableroJugador;
	private TableroOnline tableroOnline;
	
	public Ventana(String direccionIp) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		ventana = new JFrame("Tetris");
		ventana.setSize(ANCHO, ALTO);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		ventana.setLocation(screenSize.width/4, screenSize.height/4);
		
		tableroJugador = new TableroJugador(direccionIp);
		ventana.add(tableroJugador);

		
		ventana.addKeyListener(tableroJugador);
		ventana.setVisible(true);
		
		
	}
	public static void main(String[] args) {
		new Ventana("192.168.0.20");
	}

}
