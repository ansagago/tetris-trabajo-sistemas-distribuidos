import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

/*
 * Ventana que crea y muestra un tablero tableroOnline que muestra lo que hace el jugador conectado
 */

public class Ventana2 {

	private JFrame ventana;
	public static final int ANCHO = 340;
	public static final int ALTO = 700;
	private TableroJugador tableroJugador;
	private TableroOnline tableroOnline;
	
	public Ventana2() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		ventana = new JFrame("Tetris");
		ventana.setSize(ANCHO, ALTO);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		ventana.setLocation(screenSize.width*2/4, screenSize.height/4);
		
		
		tableroOnline = new TableroOnline();
		ventana.add(tableroOnline);

		ventana.setVisible(true);
		
		
	}
	public static void main(String[] args) {
		new Ventana2();
	}

}
