import java.awt.Font;
import java.awt.Label;
import javax.swing.JFrame;

/*
 * Javier Valoria Mu�oz
 * Andr�s S�enz Gal�n
 * 
 * *******Queremos presentar el trabajo en la sala de ordenadores*******
 * 
 * Para utilizar la aplicaci�n abrir servidor y cliente en ambas computadoras que vayan a conectarse
 * 
 * Ventana de inicio de la parte servidor de la aplicaci�n. Crea una Ventana2
 * 
 */

public class VentanaInicioServidor {
	
	private JFrame ventana;
	public static final int ANCHO = 750;
	public static final int ALTO = 250;
	
	public VentanaInicioServidor() {
		ventana = new JFrame("Tetris");
		ventana.setSize(ANCHO, ALTO);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		ventana.setLocationRelativeTo(null); //para centar en panatalla
		
		Label labelTitulo = new Label("TETRIS");
		labelTitulo.setAlignment(Label.CENTER);
		labelTitulo.setFont(new Font("DialogInput", Font.BOLD, 128));
		ventana.add(labelTitulo);
		ventana.setVisible(true);
		
		new Ventana2();
		
		ventana.setVisible(false);
		
	}

	public static void main(String[] args) {
		new VentanaInicioServidor();
	}

}
