import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.Timer;

/*
 * Pieza de tetris usada por los tableros
 */

public class Tetrimino {
	
	private final int anchuraTablero = 10;
	private final int alturaTablero = 20;
	private BufferedImage bloques;
	private int[][] coords;
	private Tablero tablero;
	private final int tamanoBloque = 32;
	private int movX=0;
	private int coordX;
	private int coordY;
	private Timer timer;
	private int retardo = 500;
	private boolean colision = false;
	private boolean colisionX = false;
	
	public Tetrimino(BufferedImage bloques, int[][] coords, Tablero tablero) {
		this.bloques = bloques;
		this.coords = coords;
		this.tablero = tablero;
		coordX=4;
		coordY=0;
		timer = new Timer(retardo, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				updateTimerLento();
			}
		});
		timer.start();
	}
	
	public BufferedImage getBloques() {
		return bloques;
	}
	
	public int[][] getCoords() {
		return coords;
	}
	
	public void setCoords(int[][] coords) {
		this.coords = coords;
	}
	
	public void setMovX(int x) {
		this.movX=x;
	}
	
	/*
	 * Usado en un Timer de Tablero
	 * Comprueba constantemente si ha habido colisi�n lateral de la pieza y evita su movimiento si as� ha sido
	 */
	public void update() {
		
		//Comprueba colisi�n lateral
		if((coordX+movX+coords[0].length<=anchuraTablero)&&(coordX+movX>=0)) {
			for(int i=0;i<coords.length;i++) {
				for(int j=0;j<coords[i].length;j++) {
					if(coords[i][j]!=0) {
						if(tablero.getTablero()[coordY+i][coordX+movX+j]!=0) {
							colisionX=true;
						}
					}
				}
			}
			if(!colisionX) {
				coordX+=movX;
			}
		}
		
		movX=0;
		colisionX=false;
	}
	
	/*
	 * Usado en el Timer (m�s lento) de la propia pieza
	 * Comprueba si hay colisi�n vertical de la pieza para
	 * - Crear una nueva pieza 
	 * - Actualizar el Tablero
	 * - Detectar l�neas completas
	 */
	private void updateTimerLento() {
		
		//Comprueba colisi�n en la parte inferior
		if(coordY+coords.length+1>=alturaTablero) {
			colision = true;
		}
		// Comprueba colisi�n con otras piezas
		else {
			for(int i=0;i<coords.length;i++) {
				for(int j=0;j<coords[i].length;j++) {
					if(coords[i][j]!=0) {
						if(tablero.getTablero()[coordY+i+1][coordX+j]!=0)
						{
							colision=true;
						}
					}
					
				}
			}
		}
		
		if(!colision) coordY++;
		
		//Detiene si hay colisi�n y crea nueva
		if(colision) {
			
			timer.stop();
			if(tablero instanceof TableroJugador) {
				for(int i=0;i<coords.length;i++) {
					for(int j=0;j<coords[i].length;j++) {
						if(coords[i][j]!=0) {
							tablero.getTablero()[coordY+i][coordX+j]=1;
						}
					}
				}
			}
			
			comprobarLinea();
			
			tablero.envioColisionPieza();
			
			tablero.siguientePieza();
			
		}
		
	}
	
	/*
	 * Dibuja la pieza
	 */
	public void render(Graphics g) {
		for(int i=0; i<coords.length;i++) {
			for (int j=0;j<coords[i].length;j++) {
				if(coords[i][j]!=0) {
					g.drawImage(bloques, (j+coordX)*tamanoBloque, (i+coordY)*tamanoBloque, null);
				}
			}
		}
	}
	
	/*
	 * Acelera la velocidad de bajada de la pieza
	 */
	public void bajarPieza() {
		if(retardo != 35) cambiarTimer(35);
	}
	
	/*
	 * Ralentiza la velocidad de bajada de la pieza a la habitual
	 */
	public void pararBajarPieza() {
		if(retardo != 500) cambiarTimer(500);
	}
	
	/*
	 * Actualiza el retardo del timer de la pieza
	 */
	private void cambiarTimer(int i) {
		retardo = i;
		timer.stop();
		timer = new Timer(retardo, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTimerLento();
			}
		});
		timer.start();
	}
	
	public void rotar() {
		
		int[][] rotada=null;
		rotada = transpuesta(coords);
		rotada=inversa(rotada);
		if(coordX+rotada[0].length>anchuraTablero || coordY+rotada.length+1>alturaTablero) {
			return;
		}
		for(int i=0; i<rotada.length;i++) {
			for(int j=0; j<rotada[i].length;j++) {
				if(tablero.getTablero()[coordY+i][coordX+j]!=0) {//no se puede rotar
					return;
				}
			}
		}
		coords=rotada;
	}
	
	private int[][] transpuesta(int[][] coord){
		int[][] coordTranspuesta=new int[coord[0].length][coord.length];
	    for (int x=0; x < coord.length; x++) {
	        for (int y=0; y < coord[x].length; y++) {
	          coordTranspuesta[y][x] = coord[x][y];
	        }
	      }
	    return coordTranspuesta;
	}
	private int[][] inversa(int[][] coords){
		for(int i = 0; i < coords.length/2; i++) {
		    int[] temp = coords[i];
		    coords[i] = coords[coords.length - i - 1];
		    coords[coords.length - i - 1] = temp;
		}
		return coords;
	}
	
	/*
	 * Detecta l�neas completas en el Tablero, las elimina y aumenta la puntuaci�n
	 */
	public void comprobarLinea() {
		int lineas=0;
		int altura=tablero.getTablero().length-1;
		for(int i=altura;i>0;i--) {
			int k=0; //N�mero de bloques rellenos
			for(int j=0;j<tablero.getTablero()[0].length;j++) {
				if(tablero.getTablero()[i][j]!=0) {
					k++;
				}
				if(k==10) {
					lineas++;
				}
				tablero.getTablero()[altura][j]=tablero.getTablero()[i][j];
			}
			if(k<tablero.getTablero()[0].length) {
				altura--;
			}
		}
		tablero.sumarPuntuacion(lineas);
	}
	
	
}
