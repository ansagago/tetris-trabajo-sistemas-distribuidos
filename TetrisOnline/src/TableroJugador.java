import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/*
 * Tablero establece conexi�n con un servidor de la aplicaci�n y ofrece la funcionalidad del tetris al jugador mientras se comunica con dicho servidor
 */

public class TableroJugador extends Tablero implements KeyListener{
	
	private String direccionIp = "localhost";
	private Socket socketSig=null;
	private DataOutputStream dosSig = null;
	
	public TableroJugador(String direccionIp) {
		super();
		this.direccionIp = direccionIp;
		siguientePieza();
	}
	
	/*
	 * Comienza la conexi�n con el TableroOnline si no existe ya
	 * Genera un Tetrimino aleatoriamente y env�a la informaci�n al TableroOnline para que cree el mismo
	 * Env�a el valor de la puntuaci�n actual al TableroOnline
	 * Marca fin si no hay m�s espacio para crear una nueva pieza
	 */
	@Override
	public void siguientePieza() {
		int numero=(int)(Math.random()*7);
		
		try {
			if(socketSig==null) {
				socketSig = new Socket(direccionIp, 7777);
			}
			if(dosSig==null) {
				dosSig = new DataOutputStream(socketSig.getOutputStream());
			}
			
			dosSig.writeInt(numero);
			dosSig.flush();
			dosSig.writeInt(this.puntuacion);
			dosSig.flush();
			
			
			Tetrimino nuevaPieza = getNuevaPieza(numero);
			
			for(int i = 0; i<nuevaPieza.getCoords().length;i++) {
				for(int j=0;j<nuevaPieza.getCoords()[i].length;j++) {
					if(nuevaPieza.getCoords()[i][j]!=0) {
						if(getTablero()[i][j+4] != 0) { //Si no hay hueco para una nueva pieza
							setFin(true);
							return; //Para no crear una nueva pieza
						}
					}
				}
			}
			setActual(nuevaPieza);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Env�a los datos de un KeyEvent a el TableroOnline
	 */
	public void sendKey(KeyEvent arg0) {
		Socket socketKey = null;
		DataOutputStream dosKey = null;
		
		try {

			socketKey = new Socket(direccionIp, 8888);

			dosKey = new DataOutputStream(socketKey.getOutputStream());

			
			dosKey.writeInt(arg0.getKeyCode());
			dosKey.flush();
			
			dosKey.writeInt(arg0.getID());
			dosKey.flush();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(socketKey != null) socketKey.close();
				if(dosKey != null) dosKey.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Registra la pulsaci�n de una tecla y act�a
	 *  - Flecha izquierda: mueve Tetrimino a la izquierda
	 *  - Flecha derecha: mueve Tetrimino a la derecha
	 *  - Flecha abajo: acelera la caida del Tetrimino
	 *  Llama a sendKey
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_LEFT) {
			getActual().setMovX(-1);
		}
		else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
			getActual().setMovX(1);
		}
		else if(arg0.getKeyCode() == KeyEvent.VK_DOWN) {
			getActual().bajarPieza();
		}
		
		sendKey(arg0);
	}

	/*
	 * Registra el momento en que se suelta una tecla y act�a
	 *  - Flecha abajo: detiene el movimiento acelerado del Tetrimino
	 *  - Flecha arriba: rota el Tetrimino
	 *  Llama a sendKey
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_DOWN) {
			getActual().pararBajarPieza();
		}
		else if(arg0.getKeyCode() == KeyEvent.VK_UP) {
			getActual().rotar();
		}
		
		sendKey(arg0);
	}

	@Override
	public void keyTyped(KeyEvent arg0) {}
	
	/*
	 * Env�a el estado del TableroJugador al TableroOnline para evitar desincronizaciones entre 
	 * 		las piezas que hayan podido ser causadas por problemas de conexi�n
	 */
	public void envioColisionPieza() {
			try {
				if(socketSig==null) {
					socketSig = new Socket(direccionIp, 7777);
				}

				ObjectOutputStream oos = new ObjectOutputStream(socketSig.getOutputStream());
				
				oos.writeObject(getTablero());
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		
	}

	/*
	 * Crea un nuevo Tetrimino
	 */
	@Override
	public Tetrimino getNuevaPieza(int numero) {
		Tetrimino t = null;
		switch(numero) {
			case 0: //I
				t = new Tetrimino(bloques.getSubimage(0, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1,1}},this); 
				break;
			case 1: //Z
				t = new Tetrimino(bloques.getSubimage(tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,0},{0,1,1}},this); 
				break;
			case 2: //S
				t = new Tetrimino(bloques.getSubimage(2*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{0,1,1},{1,1,0}},this);
				break;
			case 3: //J
				t = new Tetrimino(bloques.getSubimage(3*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{0,0,1}},this); 
				break;
			case 4: //L
				t = new Tetrimino(bloques.getSubimage(4*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{1,0,0}},this); 
				break;
			case 5: //T
				t = new Tetrimino(bloques.getSubimage(5*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1,1},{0,1,0}},this); 
				break;
			case 6: //O
				t = new Tetrimino(bloques.getSubimage(6*tamanoBloque, 0, tamanoBloque, tamanoBloque),
						new int[][] {{1,1},{1,1}},this); 
				break;
		}
		return t;
	}


}
