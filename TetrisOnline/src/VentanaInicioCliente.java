import java.awt.Button;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

/* 
 * Andr�s S�enz Gal�n
 * Javier Valoria Mu�oz
 * 
 * *******Queremos presentar el trabajo en la sala de ordenadores*******
 * 
 * Para utilizar la aplicaci�n abrir servidor y cliente en ambas computadoras que vayan a conectarse
 * 
 * Ventana t�tulo de la parte cliente de la aplicaci�n, que pide una ip a la que conectarse y crea una Ventana
 * 
 */

public class VentanaInicioCliente {

	private JFrame ventana;
	public static final int ANCHO = 750;
	public static final int ALTO = 700;
	
	public VentanaInicioCliente() {
		ventana = new JFrame("Tetris");
		ventana.setSize(ANCHO, ALTO);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		ventana.setLocationRelativeTo(null); //para centar en panatalla
		ventana.setLayout(new GridLayout(2, 1, 0, 0));
		
		Label labelTitulo = new Label("TETRIS");
		labelTitulo.setAlignment(Label.CENTER);
		labelTitulo.setFont(new Font("DialogInput", Font.BOLD, 128));
		ventana.add(labelTitulo);
		
		Container c = new Container();
		c.setLayout(new GridBagLayout());
		GridBagConstraints g = new GridBagConstraints();
		ventana.add(c);
		
		g.gridx = 0;
		g.gridy = 0;
		g.weighty = 0.3;
		Label labelIP = new Label("Introduzca IP");
		labelIP.setAlignment(Label.CENTER);
		labelIP.setFont(new Font("Dialog", Font.PLAIN, 24));
		c.add(labelIP, g);
		
		g.gridx = 0;
		g.gridy = 1;
		g.weighty = 0.3;
		TextField textIP = new TextField();
		textIP.setPreferredSize( new Dimension( 200, 24 ));
		c.add(textIP, g);
		
		Button buttonStart = new Button("Buscar");
		g.gridx = 0;
		g.gridy = 2;
		g.weighty = 0.6;
		c.add(buttonStart, g);
		
		buttonStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comenzar(textIP.getText());
			}
		});
		
		ventana.setVisible(true);
		
	}
	
	public void comenzar(String direccionIP) {
		new Ventana(direccionIP);
		ventana.setVisible(false);
	}

	public static void main(String[] args) {
		new VentanaInicioCliente();
	}

}