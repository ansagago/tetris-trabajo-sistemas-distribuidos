import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/*
 * Ofrece las funciones m�s b�sicas comunes a ambos tableros
 */

public abstract class Tablero extends JPanel{
	
	protected BufferedImage bloques;
	protected final int tamanoBloque = 32;
	private final int anchuraTablero = 10;
	private final int alturaTablero = 20;
	private int[][] tablero = new int[alturaTablero][anchuraTablero];
	private Tetrimino actual;
	private Timer timer;
	private final int retardo = 15;
	protected boolean fin = false;
	private boolean moverPieza = true;
	public int puntuacion=0;
	private boolean acabar = false;
	
	public Tablero() {
		
		try {
			bloques=ImageIO.read(Tablero.class.getResource("/Bloques.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		timer=new Timer();
		TimerTask task = new TimerTask() {
			public void run() {
				if(!fin) {
					if(actual!=null) {
						update();
						repaint();
					}
					
				}
				if(fin&&!acabar) { //Para redibujar una ultima vez
					update();
					repaint();
					acabar=true;
				}
				
		}};
		timer.schedule(task,0, retardo);
	}
	
	public void bloquearTeclas() {
		moverPieza = false;
	}
	
	public void desbloquearTeclas() {
		moverPieza = true;
	}
	
	public boolean teclasBloqueadas() {
		return moverPieza;
	}
	
	public int[][] getTablero(){
		return tablero;
	}
	
	public void setTablero(int[][] tablero) {
		this.tablero = tablero;
	}
	
	public Tetrimino getActual() {
		return actual;
	}
	
	public void setActual(Tetrimino t) {
		actual = t;
	}
	
	public void setFin(boolean fin) {
		this.fin = fin;
	}
	
	public void update() {
		actual.update();
	}
	
	public abstract void siguientePieza();
	
	public abstract Tetrimino getNuevaPieza(int numero);
	
	public abstract void envioColisionPieza();
	
	/*
	 * Dibuja las distintas componentes del Tablero
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		actual.render(g);
		for(int i=0;i<tablero.length;i++) {
			for(int j=0;j<tablero[i].length;j++) { //Dibujar piezas tablero
				if(tablero[i][j]!=0) {
					g.drawImage(bloques.getSubimage(7*tamanoBloque, 0, tamanoBloque, tamanoBloque), j*tamanoBloque, i*tamanoBloque,null);
				}
				
			}
		}
		
		//Dibujar lineas
		for(int i = 0; i<alturaTablero; i++) {
			g.drawLine(0, i*tamanoBloque, anchuraTablero*tamanoBloque, i*tamanoBloque);
		}
		for(int i = -1; i<anchuraTablero + 1; i++) {
			g.drawLine(i*tamanoBloque, 0, i*tamanoBloque, alturaTablero*tamanoBloque-32);
		}
		if(fin) {
			g.setFont(new Font(g.getFont().getFontName(), Font.BOLD, 80));
			g.drawString("FIN", 100, 320);
			g.setColor(Color.RED);
		}
		g.setFont(new Font(g.getFont().getFontName(), Font.BOLD, 32));
		g.drawString("Puntuacion: " + puntuacion, 10, 650);
	}
	
	public void sumarPuntuacion(int n) {
		puntuacion=puntuacion+n;
	}

	
	
}
